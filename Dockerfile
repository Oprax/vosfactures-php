FROM php:7.3-cli

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt update && apt upgrade -y
RUN apt install -y --no-install-recommends \
    build-essential \
    libmcrypt-dev \
    libfreetype6-dev \
    libpq-dev \
    libzip-dev \
    libicu-dev \
    libcurl3-dev \
    locales \
    zip \
    unzip \
    git \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install XDebug
RUN pecl install xdebug && docker-php-ext-enable xdebug
RUN echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini
RUN echo 'xdebug.remote_port=9000' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_connect_back=1' >> /usr/local/etc/php/php.ini

# Install extensions
RUN docker-php-ext-configure intl
RUN docker-php-ext-install zip iconv pcntl bcmath intl

# https://stackoverflow.com/a/58531438
RUN sed -i "s|SECLEVEL=2|SECLEVEL=1|g" /usr/lib/ssl/openssl.cnf

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www
