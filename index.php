<?php

session_start();

use GuzzleHttp\Exception\RequestException;
use Oprax\VosFactures\VosFactures;

require("vendor/autoload.php");

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$vosfactures = new VosFactures(getenv("VOSFACTURES_BASE_URL"), getenv("VOSFACTURES_CODE_API"));
try {
    $orderId = uniqid('ORDER_TEST_');
    $userId = uniqid('USER_TEST_');
    $resp = $vosfactures->createInvoice(
        [
            "kind" => "vat",
            "issue_date" => date("Y-m-d"),
            "test" => true,
            "seller_tax_no" => "FR5252445767",
            "seller_name" => "Oprax",
            "seller_email" => "contact@oprax.fr",
            "seller_www" => "https://www.oprax.fr",
            "user_id" => $userId,
            "paid" => "1000",
            "buyer_name" => "Romain Muller",
            "buyer_email" => "com@oprax.fr",
            "oid" => $orderId,
            "oid_unique" => "yes",
            "positions" => [
                [
                    "product_id" => "1",
                    "name" => "Produit A",
                    "quantity" => "1",
                    "price_net" => "59,00",
                    "tax" => "23",
                    "price_gross" => "72,57",
                    "total_price_net" => "59,00",
                    "total_price_gross" => "72,57",
                ],
                [
                    "product_id" => "2",
                    "name" => "Produit B",
                    "quantity" => "2",
                    "price_net" => "10",
                    "tax" => "10",
                    "price_gross" => "11",
                    "total_price_net" => "20",
                    "total_price_gross" => "22",
                ]
            ]
        ]
    );
//    var_dump($resp);
//    echo $resp->id;
    $vosfactures->sendInvoiceByEmail($resp->id);
} catch (RequestException $e) {
    var_dump($e->getMessage());
    $req = $e->getRequest();
    var_dump($req->getHeaders(), $req->getUri());
    if ($e->hasResponse()) {
        $resp = $e->getResponse();
        var_dump($resp->getStatusCode(), $resp->getHeaders(), $resp->getBody()->getContents());
    }
}
