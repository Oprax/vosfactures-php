<?php

namespace Oprax\VosFactures;

use GuzzleHttp\Client;
use Oprax\VosFactures\Exceptions\NotSucceedException;

class VosFactures
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $code_api;

    /**
     * Payment constructor.
     * @param string $base_url
     * @param string $code_api
     */
    public function __construct(string $base_url, string $code_api)
    {
        $this->code_api = $code_api;
        $this->client = new Client([
            "base_uri" => $base_url,
            "headers" => [
                "Accept" => "application/json",
                "Content-Type" => "application/json",
            ],
        ]);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createInvoice(array $data)
    {
        return \GuzzleHttp\json_decode($this->client->post("invoices.json", [
            "json" => [
                "api_token" => $this->code_api,
                "invoice" => $data,
            ]
        ])->getBody()->getContents());
    }

    public function createPayment(array $data)
    {
        return \GuzzleHttp\json_decode($this->client->post("banking/payments.json", [
            "json" => [
                "api_token" => $this->code_api,
                "banking_payment" => $data,
            ]
        ])->getBody()->getContents());
    }

    /**
     * @param $invoice_id
     * @throws NotSucceedException
     */
    public function sendInvoiceByEmail($invoice_id)
    {
        $resp = $this->client->post("invoices/" . $invoice_id . "/send_by_email.json", [
            "query" => ["api_token" => $this->code_api]
        ]);
        $result = \GuzzleHttp\json_decode($resp->getBody()->getContents());

        if ($result->status !== "ok") {
            throw new NotSucceedException($result->message, $this->client->getConfig("base_uri") . "invoices/" . $invoice_id . "/send_by_email.json");
        }
    }

    /**
     * @param $invoice_id
     * @return string
     */
    public function downloadInvoice($invoice_id)
    {
        $resp = $this->client->get("invoices/" . $invoice_id . ".pdf", [
            "query" => ["api_token" => $this->code_api]
        ]);
        return $resp->getBody()->getContents();
    }
}

